package com.poc.googleDrivePOC;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DigitalIDPOCController {	  
	 
	 @PostMapping("/tp/ageValidation/post")
	 @ResponseBody
	 public String receiveTPResponse(@RequestBody String mcAgeCheck) throws URISyntaxException {
		  return "Success";
	 }
	 
	 
	 @PostMapping("/mastercard/op/redirectRP") 
	 public ResponseEntity<Object>  redirectTP(HttpServletRequest request, HttpServletResponse httpServletResponse,
			 									@RequestParam("ssn") String ssn
			 									
			 									) throws URISyntaxException {
		  String address = "1234%20GrandRiver%20Ave%20Farmington%20MI%2048335";
		  String name = "John";
		  
		  URI tpapp = new URI("app://amazon?"+"ssn="+ssn+"&address="+address+"&name="+name);
		  HttpHeaders httpHeaders = new HttpHeaders(); httpHeaders.setLocation(tpapp);
		  return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
	  }

}
